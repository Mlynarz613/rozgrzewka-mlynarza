<?php
/**
 * Created by PhpStorm.
 * User: ITuser
 * Date: 2016-06-09
 * Time: 13:42
 */

function debug($var, $rodzaj = '')
{
    switch ($rodzaj) {
        case 'short':
            echo '<pre>';
            print_r($var);
            echo '<pre>';
            break;

        default:
            echo '<pre>';
            var_dump($var);
            echo '<pre>';
    }
}
?>