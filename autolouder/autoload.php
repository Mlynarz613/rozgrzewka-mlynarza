<?php
/**
 * Created by PhpStorm.
 * User: ITuser
 * Date: 2016-06-09
 * Time: 15:24
 */
function __autoload($className)
{
    $file = "./"._DIR_LIB_."/".$className._SUFIX_CLASS_;

    if(is_readable($file))
    {
        require_once $file;
    }
    else {

        throw new Exception("Nie ma takiego pliku".$file);
    }
}



